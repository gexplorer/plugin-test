angular.module('sdk.controllers', [])

    .controller('SDKCtrl', function ($scope, $ionicPopup) {
        var success = function (message) {
            console.log("* Success");

            var successPopup = $ionicPopup.alert({
                title: 'Success',
                template: message
            });

            successPopup.then(function (res) {
                console.log('  Success: ' + res);
            });
        };

        var successArray = function (data) {
            var result = "";
            for (var d in data) {
                result += d + "  - " + data[d] + "<br>";
            }
            success(result);

        };

        var failure = function (message) {
            console.log("* Failure")
            var failurePopup = $ionicPopup.alert({
                title: 'ERROR',
                template: message,
                okType: 'button-assertive'
            });
            failurePopup.then(function (res) {
                console.log('  Failure: ' + res);
            });
        };

        $scope.findDevices = function () {
            console.log("* findDevices");

            SPCFit.findDevices(successArray, failure);
        };

        $scope.connectToDevice = function () {
            console.log("* connectToDevice");
            SPCFit.connectToDevice("1234", success, failure);
        };

        $scope.disconnectFromDevice = function () {
            console.log("* disconnectFromDevice");
            SPCFit.disconnectFromDevice(success, failure);
        };

        $scope.getTime = function () {
            console.log("* getTime");
            SPCFit.getTime(success, failure);
        };

        $scope.setTime = function () {
            console.log("* setTime");
            var time = "31-8-13";
            SPCFit.setTime(time, success, failure);
        };

        $scope.getPersonalInformation = function () {
            console.log("* getPersonalInformation");

            SPCFit.getPersonalInformation(successArray, failure);
        };

        $scope.setPersonalInformation = function () {
            console.log("* setPersonalInformation");

            var sex = "M";
            var age = 32;
            var height = 160;
            var weight = 55;
            var stride = 65;

            SPCFit.setPersonalInformation(sex, age, height, weight, stride, successArray, failure);
        };

        $scope.getTotalActivityData = function () {
            console.log("* getTotalActivityData");
            var day = "31-8-13";
            SPCFit.getTotalActivityData(day, success, failure);
        };

        $scope.getDetailActivityData = function () {
            console.log("* getDetailActivityData");
            var day = "31-8-13";
            SPCFit.getDetailActivityData(day, success, failure);
        };

        $scope.getCurrentActivityInformation = function () {
            console.log("* getCurrentActivityInformation");
            SPCFit.getCurrentActivityInformation(success, failure);
        };

        $scope.deleteActivityData = function () {
            console.log("* deleteActivityData");
            var day = "31-8-13";
            SPCFit.deleteActivityData(day, success, failure);
        };

        $scope.startRealTimeMeterMode = function () {
            console.log("* startRealTimeMeterMode");

            SPCFit.startRealTimeMeterMode(success, failure);
        };

        $scope.stopRealTimeMeterMode = function () {
            console.log("* stopRealTimeMeterMode");

            SPCFit.stopRealTimeMeterMode(success, failure);
        };

        $scope.getTargetSteps = function () {
            console.log("* getTargetSteps");
            SPCFit.getTargetSteps(success, failure);
        };

        $scope.setTargetSteps = function () {
            console.log("* setTargetSteps");
            var steps = "66";
            SPCFit.setTargetSteps(steps, success, failure);
        };
    });
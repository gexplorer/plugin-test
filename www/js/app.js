angular.module('sdk', ['ionic', 'sdk.controllers'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state('sdk', {
            url: '/sdk',
            templateUrl: 'templates/sdk.html',
            controller: 'SDKCtrl'
        });

        $urlRouterProvider.otherwise('/sdk');

    });